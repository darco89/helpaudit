package pt.daa.help.repositories;


import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import pt.daa.help.entities.*;


@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>
{
}
