package pt.daa.help.repositories;


import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import pt.daa.help.entities.*;

@Repository
public interface AuditRepository extends JpaRepository<Audit, Long> 
{
}
