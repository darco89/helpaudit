package pt.daa.help.controllers;

import pt.daa.help.entities.*;
import pt.daa.help.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class QuestionController
{
	@Autowired
	private QuestionRepository questionRepository;
		
	@GetMapping("/question")
	public Question createQuestion()
	{
		Question q = new Question("NEW TITLE", "NEW DESC");
		return questionRepository.save(q);
	}
}
