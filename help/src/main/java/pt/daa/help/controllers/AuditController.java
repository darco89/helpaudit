package pt.daa.help.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import pt.daa.help.audit.AutowireHelper;
import pt.daa.help.repositories.AuditRepository;
import pt.daa.help.entities.Audit;

//I didn't want to use this controller, but I couldn't use repositories in my GenericListener's class
//(they are null all the time).
@Component
public class AuditController {
	@Autowired
	AuditRepository auditRepo;
	
	//I had to implement AutowireHelper, 
	//so that AuditRepository is not null, when I invoke createAudit() on my GenericLisntener's @PostPersist.
	@Bean
	public AutowireHelper hlpr()
	{
		return AutowireHelper.getInstance();
	}
	
	//Trying to add an Audit record, for every record persisted of any entity derived from AuditModel.
	public Audit createAudit(Audit am)
	{
		AutowireHelper.autowire(this, this.auditRepo);
		return auditRepo.save(am);
	}

}
