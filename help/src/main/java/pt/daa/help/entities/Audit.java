package pt.daa.help.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Audit")
public class Audit {

	/**
	 * 
	 */
	private static final long serialVersionUID = -614846859163023210L;

	@Id
	@GeneratedValue(generator = "sqgen_audit")
	@SequenceGenerator(name = "sqgen_audit", sequenceName = "sq_audit", initialValue = 1, allocationSize = 1)
	private Long id;

	@Column
	private String operacao;

	@Column
	private String jSonObject;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public String getjSonObject() {
		return jSonObject;
	}

	public void setjSonObject(String jSonObject) {
		this.jSonObject = jSonObject;
	}
}
