package pt.daa.help.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import pt.daa.help.audit.AuditModel;

@Entity
@Table(name = "Questions")
public class Question extends AuditModel
{
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;
	
	public Question()
	{
	}
	
	public Question(String title, String desc)
	{
		this.title=title;
		this.description=desc;
	}

	@Id
	@GeneratedValue(generator = "sqgen_question")
	@SequenceGenerator(name = "sqgen_question", sequenceName = "sq_question", initialValue = 1, allocationSize =  1)
	private Long id;

	@NotBlank
	@Size(min = 3, max = 100)
	@Column(columnDefinition = "text")
	private String title;

	@Column(columnDefinition = "text")
	private String description;
	
	/*
	 * Getters and setters
	 */
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}
	
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
