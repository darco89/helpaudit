package pt.daa.help.audit;

import java.util.Date;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pt.daa.help.controllers.AuditController;
import pt.daa.help.repositories.AuditRepository;
import pt.daa.help.entities.Audit;
import pt.daa.help.entities.Question;

@Component
public class GenericListener {
	/*
	 * #CONSTANTS
	 */
	@PrePersist
	public void prePersist(AuditModel model) {
		// These 2 properties are duly annotated dates @entity.
		// Why is spring not assuming there? I didn't want to write this code.
		model.setDataCriacao(new Date());
		model.setDataModificacao(new Date());
	}

	@PostPersist
	public void postPersist(Object target) {
		// dummy values for auditing. will be more complex later.
		String Operation = "persist";
		String json = target.getClass().getName();

		//TODO: ADD SPECIFIC CASES. 
		//is Switching-this better than having IfElses of "instance of"?
		switch (target.getClass().getName()) {
		default:
			System.out.println("Persistido objecto: " + target.toString());
			break;
		}

		//ON POST PERSIST, I WANT TO RECORD MY OPERATION, SAVING THE OBJECT'S JSON.
		Audit a = new Audit();
		a.setOperacao(Operation);
		a.setjSonObject(json);
		
		//asking controller to SAVE audit instance, because I can't access repository here.
		//I would like to use AuditRepository.save(a) instead, but I can't, even if i autowire it.
		AuditController audCtrk = new AuditController();
		audCtrk.createAudit(a);
	}
}
