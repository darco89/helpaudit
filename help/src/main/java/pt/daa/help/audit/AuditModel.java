package pt.daa.help.audit;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pt.daa.help.audit.GenericListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(GenericListener.class)
@JsonIgnoreProperties(value = { "dataCriacao", "dataModificacao" }, allowGetters = true)
public abstract class AuditModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6627813780702476083L;

	@CreatedDate
	@Column(name = "data_criacao", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;

	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_modificacao", nullable = false)
	private Date dataModificacao;

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataModificacao() {
		return dataModificacao;
	}

	public void setDataModificacao(Date dataModificacao) {
		this.dataModificacao = dataModificacao;
	}
}